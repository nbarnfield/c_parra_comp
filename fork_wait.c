#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(void){
    // Initialise i for counter, n as max limit, status as return status
    int i, n=4, status;
    // create process id variable
    pid_t childpid;
    /* On the 4th iteration the loop will be skipped 
    and the 3rd child will return 0, which changes it's state
    and the 2nd child then skips the loop because it breaks
    as does the first child and then the program finishes in its entirety */
    for(i = 1; i < n; ++i){
        // if return value is -1 throw error
        if((childpid = fork()) < 0){
            perror("error in fork");
            exit(EXIT_FAILURE);}
        // if non-negative value returned (child process ID) wait for change of state in child pid
        // child will have 0 as it's pid and therefore will go to the else clause to then create another child
        else if(childpid){
            printf("This is the %d call\n", i);
            // Monitors for a state change in the child process
            // i.e. return value of 0 or exit()
            wait(&status);
            break;
        }
        else{
            //printf("I am the %d child process in the else clause about to increment i\n", i);
        }
    }
    printf("This is process %d with parent %d\n",
    getpid(), getppid());
    return 0;
}